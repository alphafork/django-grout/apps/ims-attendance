from django.db import models
from ims_base.models import AbstractBaseDetail, AbstractLog
from reusable_models import get_model_from_string

DailySchedule = get_model_from_string("DAILY_SCHEDULE")
BatchStudent = get_model_from_string("BATCH_STUDENT")


class LeaveReason(AbstractBaseDetail):
    pass


class Attendance(AbstractLog):
    daily_schedule = models.ForeignKey(DailySchedule, on_delete=models.CASCADE)
    batch_student = models.ForeignKey(BatchStudent, on_delete=models.CASCADE)
    is_present = models.BooleanField(default=False)
    leave_reason = models.ForeignKey(
        LeaveReason, on_delete=models.CASCADE, null=True, blank=True
    )
    remarks = models.TextField(blank=True)

    def __str__(self) -> str:
        return self.daily_schedule.__str__() + " | " + self.batch_student.__str__()
