from ims_api_permission.apis import APIMethodPermission
from ims_base.apis import BaseAPIViewSet, BaseRetrieveView
from ims_user.apis import UserFilter
from reusable_models import get_model_from_string

from ims_attendance.serializers import (
    AttendanceSerializer,
    BatchAttendanceSerializer,
    RegistrationAttendanceSerializer,
)

from .models import Attendance

Subject = get_model_from_string("SUBJECT")
Course = get_model_from_string("COURSE")
StudentRegistration = get_model_from_string("STUDENT_REGISTRATION")


class AttendanceAPI(BaseAPIViewSet):
    serializer_class = AttendanceSerializer
    permission_classes = BaseAPIViewSet.permission_classes + [APIMethodPermission]
    managed_permissions = {
        "OPTIONS": ["Admin", "Manager", "Faculty"],
        "GET": ["Admin", "Manager", "Faculty"],
        "POST": ["Admin", "Manager", "Faculty"],
        "PUT": ["Admin", "Manager", "Faculty"],
        "PATCH": ["Admin", "Manager", "Faculty"],
        "DELETE": ["Admin", "Manager"],
    }


class BatchAttendanceAPI(BaseAPIViewSet):
    serializer_class = BatchAttendanceSerializer
    model_class = Attendance
    permission_classes = BaseAPIViewSet.permission_classes + [APIMethodPermission]
    managed_permissions = {
        "OPTIONS": ["Admin", "Manager", "Faculty"],
        "GET": ["Admin", "Manager", "Faculty"],
        "POST": ["Admin", "Manager", "Faculty"],
    }


class RegistrationAttendanceAPI(BaseRetrieveView):
    serializer_class = RegistrationAttendanceSerializer
    queryset = StudentRegistration.objects.all()
    filter_backends = BaseAPIViewSet.filter_backends + [UserFilter]
    user_lookup_field = "student"
