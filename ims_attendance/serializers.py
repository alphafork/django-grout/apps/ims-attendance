from datetime import datetime

from ims_base.serializers import BaseModelSerializer
from rest_framework import serializers
from reusable_models import get_model_from_string

from .models import Attendance

DailySchedule = get_model_from_string("DAILY_SCHEDULE")
StudentRegistration = get_model_from_string("STUDENT_REGISTRATION")


class AttendanceSerializer(BaseModelSerializer):
    class Meta(BaseModelSerializer.Meta):
        model = Attendance

    def validate(self, attrs):
        data = super().validate(attrs)
        daily_schedule = data["daily_schedule"]
        is_present = data["is_present"]
        leave_reason = data["leave_reason"]
        if is_present and leave_reason:
            raise serializers.ValidationError(
                {
                    "leave_reason": "Present student cant have" + " leave reason.",
                }
            )

        if (
            not self.context["request"]
            .user.groups.filter(name__in=["Manager", "Admin"])
            .exists()
            and daily_schedule.date != datetime.today().date()
        ):
            raise serializers.ValidationError(
                {
                    "daily_schedule": "You have no permission"
                    + " to take attendance for this date.",
                }
            )
        return data


class BatchAttendanceSerializer(BaseModelSerializer):
    attendance = serializers.ListField(
        write_only=True,
        child=AttendanceSerializer(),
    )
    daily_schedule = serializers.PrimaryKeyRelatedField(
        write_only=True,
        queryset=DailySchedule.objects.all(),
    )

    class Meta(BaseModelSerializer.Meta):
        fields = ["attendance", "daily_schedule"]

    def validate(self, attrs):
        validation_errors = {}
        validated_data = super().validate(attrs)
        class_attendance = validated_data.get("attendance", [])
        daily_schedule = validated_data.get("daily_schedule", None)
        if not class_attendance:
            validation_errors.update(
                {"class_attendance": "This field can not be empty."}
            )
        if not daily_schedule:
            validation_errors.update({"daily_schedule": "This field can not be empty."})

        for student_attendance in class_attendance:
            if student_attendance["daily_schedule"] != daily_schedule:
                validation_errors.update(
                    {
                        "attendance": student_attendance["batch_student"].__str__()
                        + "entry conflicts with given schedule."
                    }
                )
        if validation_errors:
            raise serializers.ValidationError(validation_errors)
        return validated_data

    def create(self, validated_data):
        class_attendance = validated_data.pop("attendance", [])
        daily_schedule = validated_data.pop("daily_schedule", [])
        old_attendance = Attendance.objects.filter(daily_schedule=daily_schedule)
        if old_attendance.exists():
            old_attendance.delete()
        for student_attendance in class_attendance:
            Attendance.objects.create(**student_attendance)
        return True


class RegistrationAttendanceSerializer(BaseModelSerializer):
    class Meta(BaseModelSerializer.Meta):
        model = StudentRegistration

    def to_representation(self, instance):
        registration_attendance = Attendance.objects.filter(
            batch_student__registration=instance
        )
        present_days = 0
        total_days = 0
        for daily_attendance in registration_attendance:
            total_days += 1
            if daily_attendance.is_present:
                present_days += 1

        if total_days:
            attendance_precentage = f"{round(present_days*(100/total_days), 2)}%"
        else:
            attendance_precentage = "Not Applicable"
        return {
            "total_days": total_days,
            "present_days": present_days,
            "absent_days": total_days - present_days,
            "attendance_precentage": attendance_precentage,
        }
