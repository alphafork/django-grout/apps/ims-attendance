from ims_attendance.models import Attendance


def get_course_attendance(registration, course):
    registration_attendance = Attendance.objects.filter(
        batch_student__registration=registration,
        batch_student__batch__course=course,
    )
    present_days = 0
    total_days = 0
    for daily_attendance in registration_attendance:
        total_days += 1
        if daily_attendance.is_present:
            present_days += 1

    if total_days:
        attendance_percentage = f"{round(present_days*(100/total_days), 2)}%"
        attendance = {
            "total_days": total_days,
            "present_days": present_days,
            "absent_days": total_days - present_days,
            "attendance_percentage": attendance_percentage,
        }
    else:
        attendance = "Not Applicable"
    return {"attendance": attendance}
