from django.urls import include, path
from rest_framework import routers
from rest_framework.urlpatterns import format_suffix_patterns

from .apis import BatchAttendanceAPI, RegistrationAttendanceAPI

router = routers.SimpleRouter()
router.register(r"batch-attendance", BatchAttendanceAPI, "batch-attendance")

urlpatterns = [
    path("registration/<int:pk>/", RegistrationAttendanceAPI.as_view()),
    path("", include(router.urls)),
]

urlpatterns = format_suffix_patterns(urlpatterns, allowed=["json"])
